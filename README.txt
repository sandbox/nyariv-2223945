Isotopia
-------------
Isotopia is a site-builder friendly module for setting up jQuery Isotope on the site.

## Dependencies
* libraries

## Installation
* Download the latest version of [jquery.isotope.min.js][jquery.isotope]
* Place jquery.isotope.min.js in sites/all/libraries/isotope/

## Note

[jquery.isotope]: http://isotope.metafizzy.co/jquery.isotope.min.js